#!/bin/bash

# check if helm is installed
helm version
if [ $? -gt 0 ]; then
    # install helm on mac
    brew install helm
fi

# add rbac
kubectl create serviceaccount tiller --namespace=kube-system
kubectl create clusterrolebinding tiller-admin --serviceaccount=kube-system:tiller --clusterrole=cluster-admin

# for new helm installs only
helm init --service-account=tiller --history-max 200

# helm already installed?
# run with the update flag
# helm init --service-account=tiller --history-max 200 --update

# for added security uncomment the line below
# helm init upgrade --tiller-tls-verify

# update the repo
helm repo update